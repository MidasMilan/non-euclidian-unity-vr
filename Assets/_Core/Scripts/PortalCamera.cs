﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    private static bool VRMode = false;

    [SerializeField] private Transform player;
    [SerializeField] private Transform playerCamera;
    [SerializeField] private Transform portal;
    [SerializeField] private Transform destinationPortal;
    [SerializeField] private Transform destinationCamera;

    [SerializeField] private bool debugMode = false;

    private void OnDrawGizmosSelected()
    {
        if (!debugMode) return;
        float diffAngle = 180 - Vector3.Angle(portal.forward, destinationPortal.forward * -1);
        Vector3 portalToPlayer = VRMode ? (portal.position - new Vector3(playerCamera.position.x, 0.0f, playerCamera.position.z)) : (portal.position - player.position);
        Vector3 destinationNewCamPos = destinationPortal.position - portalToPlayer;

        //Debug.Log("angle: " + Vector3.Angle(portalToPlayer, portal.right));
        Gizmos.color = Color.red;
        Gizmos.DrawLine(VRMode ? (new Vector3(playerCamera.position.x, 0.0f, playerCamera.position.z)) : (player.position), portal.position);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(portal.position, portalToPlayer);

        //Gizmos.DrawLine(destinationPortal.position, newCamPos);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(destinationPortal.position, destinationNewCamPos);
    }

    private void LateUpdate()
    {
        float diffAngle = 180 - Vector3.Angle(portal.forward, destinationPortal.forward * -1);
        Vector3 portalToPlayer = VRMode ? (portal.position - new Vector3(playerCamera.position.x, 0.0f, playerCamera.position.z)) : (portal.position - player.position);

        portalToPlayer = new Vector3(portalToPlayer.x, -playerCamera.position.y, portalToPlayer.z);
        destinationCamera.position = destinationPortal.position - portalToPlayer;

        //Replace diffAngle + 180 with proper crossproduct check
        Quaternion playerViewRotateDiff = Quaternion.Euler(0.0f, diffAngle + 180, 0.0f);
        transform.rotation = /*behind ? Quaternion.Inverse(playerViewRotateDiff) * playerCamera.rotation : */playerViewRotateDiff * playerCamera.rotation;
        //destinationCamera.rotation = Quaternion.LookRotation(destinationPortal.forward, destinationPortal.up);
        //destinationCamera.Rotate(rotateDiff.eulerAngles);

        //Vector3 newCamDirection = rotateDiff * playerCamera.forward;
        //destinationCamera.Rotate(Quaternion.LookRotation(newCamDirection, Vector3.up).eulerAngles);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class PortalBehaviour : MonoBehaviour
{
    [SerializeField] private Transform destination;
    [SerializeField] private GameObject vrPlayArea;
    [SerializeField] private Transform gizmoObject;

    private Vector3 previousPos;

    [SerializeField] private bool debugMode = false;

    private enum PortalOrientation
    {
        Z_Negative,
        Z_Positive,
        X_Negative,
        X_Positive
    };
    private PortalOrientation portalNormal;

    private void OnDrawGizmosSelected()
    {
        if (!debugMode) return;
        float angleDiff = 180 - Vector3.Angle(destination.forward, transform.forward);
        bool leftRotation = Vector3.Cross(destination.forward, transform.forward).y > 0.0f;
        Quaternion rotation = Quaternion.Euler(0.0f, leftRotation ? -angleDiff : angleDiff, 0.0f);
        Vector3 teleportPos = gizmoObject.position - transform.position;
        Vector3 rotatedTelePos = rotation * teleportPos;
        Gizmos.color = Color.green;
        Gizmos.DrawLine(gizmoObject.position, transform.position);

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(destination.position - teleportPos, destination.position);

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(destination.position - rotatedTelePos, destination.position);
    }

    private void Start()
    {
        previousPos = transform.position;
        SetNewNormalAngle();
    }

    private void Update()
    {
        if(previousPos != transform.position)
        {
            SetNewNormalAngle();
            previousPos = transform.position;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        float angleDiff = 180 - Vector3.Angle(destination.forward, transform.forward);
        bool leftRotation = Vector3.Cross(destination.forward, transform.forward).y > 0.0f;
        Quaternion rotation = Quaternion.Euler(0.0f, leftRotation?angleDiff:-angleDiff, 0.0f);
        switch (portalNormal)
        {
            case PortalOrientation.Z_Negative:
                if (other.transform.position.z > transform.position.z) { teleportObject(other.gameObject, rotation, true); }
                break;
            case PortalOrientation.Z_Positive:
                if (other.transform.position.z < transform.position.z) { teleportObject(other.gameObject, rotation, true); }
                break;
            case PortalOrientation.X_Negative:
                if (other.transform.position.x > transform.position.x) { teleportObject(other.gameObject, rotation, false); }
                break;
            case PortalOrientation.X_Positive:
                if (other.transform.position.x < transform.position.x) { teleportObject(other.gameObject, rotation, false); }
                break;
            default:
                Debug.LogError("PortalOrientation not recognized!");
                break;
        }
    }

    private void teleportObject(GameObject teleportingObject, Quaternion rotationDiff, bool PortalOrientationIsZ)
    {
        if(teleportingObject.CompareTag("Player")) { teleportingObject = vrPlayArea; }
        if(teleportingObject.CompareTag("Terrain")) { return; }
        Vector3 teleportPos = transform.position - teleportingObject.transform.position;
        
        Vector3 destinationPos = destination.position - Quaternion.Inverse(rotationDiff) * teleportPos;
        if (debugMode) print("pos diff: [" + teleportPos.x + ", " + teleportPos.y + ", " + teleportPos.z + "]" + ", destinationPos: " + destination.position + ", new destination: [" + destinationPos.x + ", " + destinationPos.y + ", " + destinationPos.z + "]");
        teleportingObject.transform.SetPositionAndRotation(destinationPos, teleportingObject.transform.rotation);
        teleportingObject.transform.Rotate(rotationDiff.eulerAngles);
    }

    private void SetNewNormalAngle()
    {
        Vector3 normalVector = transform.forward * -1;
        int normalZ = Mathf.RoundToInt(normalVector.z);
        int normalX = Mathf.RoundToInt(normalVector.x);
        if(debugMode) print(gameObject.name + " pos: " + transform.position + ", normal:" + normalVector);

        if (normalZ > 0)
        {
            if (debugMode) print(gameObject.name + " set normal to Z_Negative");
            portalNormal = PortalOrientation.Z_Negative;
        }
        else if (normalZ < 0)
        {
            if (debugMode) print(gameObject.name + " set normal to Z_Positive");
            portalNormal = PortalOrientation.Z_Positive;
        }
        else if (normalX < 0)
        {
            if (debugMode) print(gameObject.name + " set normal to X_Positive");
            portalNormal = PortalOrientation.X_Positive;
        }
        else if (normalX > 0)
        {
            if (debugMode) print(gameObject.name + " set normal to X_Negative");
            portalNormal = PortalOrientation.X_Negative;
        }
    }
}

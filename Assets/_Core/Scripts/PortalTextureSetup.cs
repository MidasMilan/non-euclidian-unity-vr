﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class PortalTextureSetup : MonoBehaviour
{
    private Camera thisCam;
    private static readonly bool VRMode = true;
    [SerializeField] private Shader cutOutShader;
    [SerializeField] private MeshRenderer targetMeshRenderer;
    [SerializeField] private int antiAliasing = 2;
    [SerializeField] private int targetFPS = 144;

    private Material generatedMat;

    void Start()
    {
        Application.targetFrameRate = targetFPS;
        Cursor.visible = false;

        thisCam = GetComponent<Camera>();
        if (thisCam.targetTexture != null)
        {
            thisCam.targetTexture.Release();
        }
        thisCam.depth = Camera.main.depth + 1;
        
        RenderTexture newTargetTex = new RenderTexture(VRMode ? 1440*2 : Screen.width, VRMode ? 1600 : Screen.height, 24);
        //newTargetTex.antiAliasing = antiAliasing;
        thisCam.targetTexture = newTargetTex;

        generatedMat = new Material(cutOutShader);
        generatedMat.mainTexture = thisCam.targetTexture;
        targetMeshRenderer.material = generatedMat;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float moveSpeed = 1.0f;
    [SerializeField] private float rotationSpeed = 1.0f;

    [SerializeField] private Camera playerCamera;

    private Rigidbody rb;
    private float vertLookAngle = 0f;

    // Start is called before the first frame update
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody>();

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 movementInput = new Vector3(Input.GetAxisRaw("Horizontal"), 0.0f, Input.GetAxisRaw("Vertical"));
        Vector3 inputDirection = (transform.forward * movementInput.z) + (transform.right * movementInput.x);
        if (inputDirection.magnitude > 1.0f)//diagonal input on keyboard
        {
            inputDirection = inputDirection.normalized;
        }
        inputDirection *= moveSpeed * Time.deltaTime;

        float vertLookRot = Input.GetAxisRaw("Mouse Y") * rotationSpeed * Time.deltaTime;
        vertLookAngle -= vertLookRot;
        vertLookAngle = Mathf.Clamp(vertLookAngle, -90f, 90f);
        playerCamera.transform.localRotation = Quaternion.Euler(vertLookAngle, 0f, 0f);
        
        float horiRot = Input.GetAxisRaw("Mouse X") * rotationSpeed * Time.deltaTime;
        transform.Rotate(Vector3.up, horiRot);
        rb.MovePosition(transform.position + inputDirection);
    }
}
